/**
 * Clientside helper functions
 */

$(document).ready(function() {
  var amounts = document.getElementsByClassName("amount");

  // iterate through all "amount" elements and convert from cents to dollars
  for (var i = 0; i < amounts.length; i++) {
    amount = amounts[i].getAttribute('data-amount') / 100;  
    amounts[i].innerHTML = amount.toFixed(2);
  }

  // Initialize the Stripe connection using the public key
  const stripe = Stripe('pk_test_51KdE68D7EUhDayBtlf5YsmmiGpXOSzQBH2Pe7sPEFGOdIKdEtgo5jRjZCum4YtfSTSO16glLK0X2RD3vdkeClxnh009FLaboJC');

  /*************
   * Handle all the Checkout page funcationality
   *************/
  const formEl = document.getElementById('payment-form');
  const payEl = document.getElementById('payment-element');

  if (formEl) {
    const paymentSecret = formEl.dataset.secret;

    const options = {
      clientSecret: paymentSecret
    };
    
    // Set up Stripe.js and Elements to use in checkout form, passing the client secret obtained in step 2
    const elements = stripe.elements(options);

    // Create and mount the Payment Element
    const paymentElement = elements.create('payment');
    paymentElement.mount(payEl);

    // Handle the submittion of the Stripe payment form
    formEl.addEventListener('submit', async (event) => {
      event.preventDefault();
    
      const {error} = await stripe.confirmPayment({
        //`Elements` instance that was used to create the Payment Element
        elements,
        confirmParams: {
          return_url: window.location.origin + '/success',
        },
      });
    
      if (error) {
        // This point will only be reached if there is an immediate error when
        // confirming the payment. Show error to your customer (for example, payment
        // details incomplete)
        const messageContainer = document.querySelector('#error-message');
        messageContainer.textContent = error.message;
      } else {
        // Your customer will be redirected to your `return_url`. For some payment
        // methods like iDEAL, your customer will be redirected to an intermediate
        // site first to authorize the payment, then redirected to the `return_url`.
      }
    });
  }

  /*************
   * Handle all the payment processing feedback page functionality
   *************/
  const successEl = document.getElementById('payment-response');
  const amountEl = document.getElementById('payment-amount');
  const paymentIdEl = document.getElementById('payment-id');

  if (successEl) {
    // Retrieve the "payment_intent_client_secret" query parameter appended to
    // your return_url by Stripe.js
    const paymentSecret = new URLSearchParams(window.location.search).get(
      'payment_intent_client_secret'
    );

    // Retrieve the PaymentIntent
    stripe.retrievePaymentIntent(paymentSecret).then(({paymentIntent}) => {
      const message = document.querySelector('#message');

      const amount = (paymentIntent.amount / 100).toFixed(2);
      amountEl.innerText = amount;
      paymentIdEl.innerText = paymentIntent.id;

      // Inspect the PaymentIntent `status` to indicate the status of the payment
      // to your customer.
      //
      // Some payment methods will [immediately succeed or fail][0] upon
      // confirmation, while others will first enter a `processing` state.
      //
      // [0]: https://stripe.com/docs/payments/payment-methods#payment-notification
      $('#payment-success').addClass('d-none');
      $('#payment-failure').addClass('d-none');
      switch (paymentIntent.status) {
        case 'succeeded':
          message.innerText = 'Success! Payment received.\n Total amount: $' + amount;
          $('#payment-success').addClass('d-block');
          break;

        case 'processing':
          message.innerText = "Payment processing. We'll update you when payment is received.";
          break;

        case 'requires_payment_method':
          message.innerText = 'Payment failed. Please try another payment method.';
          $('#payment-failure').addClass('d-block');
          // Redirect your user back to your payment page to attempt collecting
          // payment again
          break;

        default:
          message.innerText = 'Something went wrong.';
          break;
      }
    });
  }
})
